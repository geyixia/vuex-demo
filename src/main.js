import Vue from 'vue'
import App from './App.vue'
// 1. 用es6模块化引入vuex中的store 嵌入vue中
import store from './store/index.js'
Vue.config.productionTip = false

Vue.config.productionTip = false
// 2. 给Vue配置项中添加一个特殊的配置，
// 名是：store,值是上面的引入的 store
new Vue({
  store, // store: store
  render: h => h(App),
}).$mount('#app')
