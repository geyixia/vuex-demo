// 用来创建vuex的地方
import Vue from 'vue'
import Vuex from 'vuex'
// 由于Vuex是vue插件，还要以vue插件的方式挂载在vue上
// 1. 以插件的方式挂载在vue上
Vue.use(Vuex)
// 2. 创建Vuex.Store的实例
// Vuex是一个对象，其中有一个属性名是Store.
// Vuex.Store在这里当作构造器
const store = new Vuex.Store({
  // 保存公共数据state 固定写法
  state: {
    num: 1,
    info:{
      name:'tom' // this.$store.state.info.name
    },
    msg: 'hello,vuex'
  },
  mutations: { // 翻译为变化
      // 用于修改vuex中公共数据的值 两个参数第一个是我们的公共数据 第二个可选自己定义的参数 方法的参数只能是两个 一个参数是当前要修改的属性 另一个参数是可选的接收一个自定义的参数(数，数组，对象) 接收不到第三个参数 如要传多个参数可以传递对象或数组
    mAdd1 (state) { // 调用它时 形参会自动接收这个形参
        state.num += 1
    },
    mAddn (state, n) { // 调用它时 形参会自动接收这个形参
        state.num += n
    },
    mSub1 (state, n) {
        state.num -= n
    },
    mSubn (state, n) {
        state.num -= n
    },
    f1 () {console.log("我是vuex中的f1")},
    // mNum (state, n) {state.num = n}
    mNum (state, e) {state.num = e.target.value}
  }
})

// 3. 导出store
export default store